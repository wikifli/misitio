from django.http import HttpResponse
import datetime

def hola(request):
    return HttpResponse("Hola mundo")

def fecha_actual(request):
    ahora = datetime.datetime.now()
    html = "<html><body><h1>Fecha:</h1><h3>%s</h3></body></html>" % ahora
    return HttpResponse(html)

def horas_adelante(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now()+datetime.timedelta(hours=offset)
    html = "<html><body><h1>En %s hora(s), seran:</h1><h3>%s</h3></body></html>" %(offset, dt)
    return HttpResponse(html)

def atributos_meta(request):
    valor = request.META.items()
    valor.sort()
    html=[]
    for k,v in valor:
        html.append('<tr><td>%s</td><td>%s</td></tr>'%(k,v))
    return HttpResponse('<table>%s</table>'%'\n'.join(html))