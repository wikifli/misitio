
from django.conf.urls import url
from django.contrib import admin
from misitio.views import hola, fecha_actual, horas_adelante, atributos_meta
from biblioteca import views as Bviews

urlpatterns = [
     url(r'^admin/', admin.site.urls),
    url(r'^hola/$',hola),
    url(r'^fecha/$',fecha_actual),
    url(r'^fecha/mas/(\d{1,2})/$',horas_adelante),
    url(r'^params/$',atributos_meta),
    url(r'^formulario-buscar/$',Bviews.formulario_buscar),
    url(r'^buscar/$',Bviews.buscar),
]
