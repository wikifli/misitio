# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def formulario_buscar(request):
    return render(request, 'formulario_buscar.html')

def buscar(request):
    if 'q' in request.GET and request.GET['q']:
        mensaje = 'Estas buscando: %r' % request.GET['q']
    else:
        mensaje='Has subido un formulario vacio'
    return HttpResponse(mensaje)